TEs in green algae
==================

Repeat annotation in lichen-forming and non-lichen-forming green algae genomes.

The pipeline follows the [analysis by Petersen et al. (2019)][petersen2019].
The main difference is in how the repeat library is constructed. We are
building a super-library using [EDTA][edta] ([Ou et al. 2019][ou2019])

[petersen2019]: https://github.com/mptrsen/mobilome/
[edta]: https://github.com/oushujun/EDTA/
[ou2019]: https://genomebiology.biomedcentral.com/articles/10.1186/s13059-019-1905-y

Test run
--------

Using the *Ostreococcus tauri* genome version RCC4221 downloaded from [here](https://bioinformatics.psb.ugent.be/gdb/O.tauri/).


Resources
---------

- Dfam database v3.6 (curated only) from https://www.dfam.org/releases/Dfam_3.6/families

Genome assemblies
-----------------

Sometimes several samples per species.

- *Apatococcus fuscideae*
- *Apatococcus lobatus*
- *Asterochloris glomerata*
- *Auxenochlorella protothecoides*
- *Bathycoccus prasinos*
- *Botryococcus braunii*
- *Chlamydomonas reinhardtii*
- *Chlorella sorokiniana*
- *Chlorella variabilis*
- *Chromochloris zofingiensis*
- *Coccomyxa pringsheimii*
- *Coccomyxa subellipsoidea*
- *Duneliella salina*
- *Elliptochloris bilobata*
- *Helicosporidium sp.*
- *Micractinium conductrix*
- *Micromonas pusilla*
- *Monoraphidium neglectum*
- *Myrmecia bisecta*
- *Ostreococcus lucimarinus*
- *Ostreococcus tauri*
- *Parachlorella kessleri*
- *Picochlorum sp.*
- *Prasinoderma coloniale*
- *Prototheca wickerhamii*
- *Symbiochloris irregularis*
- *Trebouxia sp*
- *Ulva mutabilis*
- *Ulva prolifera*
- *Volvox carteri*
