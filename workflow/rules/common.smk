import pandas as pd
import datetime
import os
import sys
from snakemake.utils import validate

configfile: "config/config.yaml"
validate(config, "../schemas/config.schema.yaml")

samples = pd.read_csv(config["samples"], sep = "\t", dtype = str, comment = "#").set_index("sample", drop = False)
validate(samples, "../schemas/samples.schema.yaml")

samples_list = samples["sample"].tolist()
genome_sizes = dict(zip(samples["sample"], samples["genome_size_mb"].astype(float) * 1000000)) # genome size in Mbp