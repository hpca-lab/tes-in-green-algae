import os
from snakemake.remote.HTTP import RemoteProvider as HTTPRemoteProvider
HTTP = HTTPRemoteProvider()

localrules: download_dfam
rule download_dfam:
  input:
    HTTP.remote(config["dfam_url"], keep_local = False)
  output:
    hmm = "resources/Dfam_curatedonly.hmm"
  run:
    shell("gunzip -c {input} > {output}")


localrules: hmmpress
rule hmmpress:
  input:
    rules.download_dfam.output.hmm
  output:
    h3f = "resources/Dfam_curatedonly.hmm.h3f"
  log:
    "logs/hmmpress.log"
  params:
    extra = ""
  threads: 4
  wrapper:
    "0.75.0/bio/hmmer/hmmpress"
    

# From https://snakemake-wrappers.readthedocs.io/en/0.75.0/wrappers/hmmer/hmmscan.html
rule search_in_dfam:
  input:
    fasta = rules.dedupe.output.fa,
    profile = rules.hmmpress.output.h3f
  output:
    tblout = "results/030_dfam_scan/{sample}/{sample}.dfam.out.tbl",
    domtblout = "results/030_dfam_scan/{sample}/{sample}.dfam.out.domtbl",
    outfile = "results/030_dfam_scan/{sample}/{sample}.dfam.out",
  log:
    "logs/hmmscan/{sample}.hmmscan.log"
  benchmark:
    "benchmarks/search_in_dfam/{sample}.hmmscan.benchmark.txt"
  params:
    evalue_threshold = config["hmmscan_evalue_threshold"],
    # if bitscore threshold provided, hmmscan will use that instead
    #score_threshold = 50,
    extra = ""
  threads:
    12
  resources:
    walltime = "20:00"
  wrapper:
    "0.75.0/bio/hmmer/hmmscan"

localrules: extract_dfam_hits
rule extract_dfam_hits:
  input:
    rules.search_in_dfam.output.domtblout
  output:
    fasta = "results/030_dfam_scan/{sample}.dfam.hits.fa"
  conda:
    "../envs/biopython.yaml"
  script:
    "../scripts/extract_hmmscan_hits.py"
