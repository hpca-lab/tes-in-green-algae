# reformats headers by giving them sequential numbers. also writes a dictionary
# for later back-replacement.
localrules: reformat_headers
rule reformat_headers:
  input:
    fasta = get_input_fasta
  output:
    fasta = "data/clean/{sample}.enum.fa",
    dictionary =  "data/clean/{sample}.dict"
  script:
    "../scripts/make_dictionary.py"


# run EDTA
rule edta:
  input:
    fasta = rules.reformat_headers.output.fasta
  output:
    lib = "results/040_edta/{sample}/{sample}.enum.fa.mod.EDTA.TElib.fa",
    gff = "results/040_edta/{sample}/{sample}.enum.fa.mod.EDTA.TEanno.gff3",
    cat = "results/040_edta/{sample}/{sample}.enum.fa.mod.EDTA.anno/{sample}.enum.fa.mod.cat.gz",
    tbl = "results/040_edta/{sample}/{sample}.enum.fa.mod.EDTA.anno/{sample}.enum.fa.mod.tbl",
  log:
    "logs/edta/{sample}/{sample}.edta.log"
  benchmark:
    "benchmarks/edta/{sample}.edta.benchmark.txt"
  conda:
    "../envs/edta.yaml"
  threads: 24
  params:
    extra = "--overwrite 0 --sensitive 1 --anno 1 --evaluate 1 --force 1"
  shell:
    """
    full_path=$(readlink -f {input.fasta})
    log_path=$(readlink -f {log})
    output_dir=$(dirname {output.lib})
    mkdir -p ${{output_dir}}
    cd ${{output_dir}}
    EDTA.pl --threads {threads} --genome ${{full_path}} {params.extra} > ${{log_path}}
    """


localrules: replace_ids
# reformats IDs in GFF files from the dictionary created in reformat_headers.
rule replace_ids:
  input:
    tsv = rules.edta.output.gff,
    dictionary = rules.reformat_headers.output.dictionary
  output:
    tsv =  "results/050_replace_ids/{sample}/{sample}.fa.mod.EDTA.TEanno.gff3"
  script:
    "../scripts/dictionary_replace_gff.py"


localrules: extract_align
rule extract_align:
  input:
    rules.edta.output.cat
  output:
    align = "results/040_edta/{sample}/{sample}.enum.fa.mod.EDTA.anno/{sample}.enum.fa.mod.align"
  shell:
    "zcat {input} > {output}"
