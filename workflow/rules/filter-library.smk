import os
from snakemake.remote.HTTP import RemoteProvider as HTTPRemoteProvider
HTTP = HTTPRemoteProvider()


# TODO implement reallyrepeats.sh: 
# 1. BLAST search
# 2. Filter Blast result
# 3. Generate keep-these-headers.txt
rule dedupe:
  input:
    fa = rules.repeatmodeler.output.families
  output:
    fa = "results/020_deduplicated_library/{sample}.dedup.fa"
  conda:
    "../envs/te-annotation.yaml"
  resources:
    walltime = "10:00"
  shell:
    """
    cd-hit-est -d 0 -aS 0.8 -c 0.8 -G 0 -g 1 -b 500 -i {input.fa} -o {output.fa}
    """
  

# this requires that you have an existing Diamond database for the NCBI nr database.
# Download the nr database from here: https://ftp.ncbi.nlm.nih.gov/blast/db/FASTA/nr.gz
# Create the db following this guide (including taxonomy information): https://github.com/bbuchfink/diamond/wiki/3.-Command-line-options#makedb-options
rule diamond_blastx:
  input:
    fasta = rules.dedupe.output.fa
  output:
    "results/030_blast_nr/{sample}.tsv.gz"
  log:
    "logs/blastx/{sample}.diamond.log"
  conda:
    "../envs/diamond.yaml"
  threads:
    8
  params:
    db = config["nr_db"],
    outfmt = "7",
    extra = "--masking 0 --compress 1"
  shell:
    """
    diamond blastx \
        --query {input.fasta} \
        --db {params.db} \
        --threads {threads} \
        --log \
        --outfmt {params.outfmt} \
        {params.extra} > {log}
    """


localrules: filter_library
rule filter_library:
  input:
    lib = rules.repeatmodeler.output.families,
    whitelist =  "results/020_filter_library/{sample}/keep-these-headers.txt"
  output:
    fa = "results/020_filter_library/{sample}.filtered_library.fa"
  conda:
    "../envs/te-annotation.yaml"
  params:
    libraryfilter = "code/reallyrepeats.sh",
    nr_library = config["ncbi_nr_library"],
    keywords = config["keywords_file"]
  shell:
    "seqkit grep --pattern-file {input.whitelist} --by-name {input.lib} > {output.fa}"
