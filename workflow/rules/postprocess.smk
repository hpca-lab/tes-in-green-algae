localrules: create_divsum, replace_te_shorthands, extract_divergence_table
localrules: combine_divergence_tables

rule create_divsum:
    input: rules.extract_align.output.align
    output:
        divsum="results/060_repeat_landscapes/{sample}/{sample}.divsum"
    conda:
        "../envs/edta.yaml"
    shell:
        """
        perl -I $(dirname $(which calcDivergenceFromAlign.pl))/../share/RepeatMasker $(which calcDivergenceFromAlign.pl) -s {output.divsum} {input} 
        """


rule replace_te_shorthands:
    input:
        txt = rules.create_divsum.output.divsum,
        dictionary = config["te_names_replacement_table"],
    output:
        txt = "results/060_repeat_landscapes/{sample}/{sample}.divsum.names_replaced"
    script:
        "../scripts/dictionary_replace_general.py"


# this is the new rule that actually extracts *all* divergence data from the divsum files
# created by calcDivergenceFromAlign.pl
rule extract_divergence_table:
    input:
        divsum = rules.replace_te_shorthands.output.txt
    output:
        tsv = "results/060_repeat_landscapes/{sample}/{sample}.landscape_data.tsv"
    params:
        genome_size = lambda wildcards: genome_sizes[wildcards.sample]
    script:
        "../scripts/extract_divergence_table.py"


rule combine_divergence_tables:
    input:
        expand("results/060_repeat_landscapes/{sample}/{sample}.landscape_data.tsv", sample = samples_list)
    output:
        tsv = "results/070_coverage_tables_combined/all_samples.coverage.tsv"
    conda:
        "../envs/postprocess.yaml"
    script:
        "../scripts/create_large_table.R"