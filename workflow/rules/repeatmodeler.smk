from pathlib import Path

def get_input_fasta(wildcards):
  return samples.loc[samples["sample"] == wildcards.sample]["assembly"]

localrules: build_repeatmodeler_db
rule build_repeatmodeler_db:
  input:
    get_input_fasta
  output:
    db = "results/010_repeatmodeler/{sample}/{sample}-rm.db"
  log:
    "logs/repeatmodeler/{sample}.builddb.log"
  conda:
    "../envs/te-annotation.yaml"
  shell:
    "BuildDatabase -name {output} {input} > {log} && touch {output}"
    
def get_walltime(wildcards, attempt):
  hours = attempt * 24
  return f"{hours}:00:00"

rule repeatmodeler:
  input:
    rules.build_repeatmodeler_db.output.db
  output:
    families = "results/010_repeatmodeler/{sample}/{sample}-rm.db-families.fa"
  log:
    "logs/repeatmodeler/{sample}.rmodeler.log"
  benchmark:
    "benchmarks/repeatmodeler/{sample}.repeatmodeler.benchmark.txt"
  threads:
    config["repeatmodeler_threads"] * 4 # stupidly, we need to allocate more threads for repeatmodeler
  resources:
    walltime = get_walltime
  conda:
    "../envs/te-annotation.yaml"
  params:
    real_threads = config["repeatmodeler_threads"] # this is the 4x smaller number of threads
  shell:
    "RepeatModeler -pa {params.real_threads} -engine ncbi -database {input} > {log}"

