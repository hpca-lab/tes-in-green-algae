# replaces sequence ids in GFF/BED files from a tab-separated dictionary
import sys
import csv
import re

if "snakemake" in locals():
    in_file    = str(snakemake.input.txt)
    in_dict    = str(snakemake.input.dictionary)
    out_file   = str(snakemake.output.txt)
else:
    if len(sys.argv) < 3: sys.exit("Needs three arguments: 1) input file, 2) input dictionary, 3) output file")
    in_file   = sys.argv[1]
    in_dict   = sys.argv[2]
    out_file  = sys.argv[3]


def main(in_file, in_dict, out_file):
    # slurp dictionary
    replacement_for = dict()
    with open(in_dict, "r") as infh:
        reader = csv.reader(infh, delimiter="\t")
        for row in reader:
            replacement_for[row[0]] = row[1] # key in column 1, value (trimmed) in column 2


    with open(in_file, "r") as infh, open(out_file, "w") as outfh:
        for row in infh:
            for pattern in replacement_for:
                row = re.sub(pattern, replacement_for[pattern], row)
            outfh.write(row)


if __name__ == "__main__":
    main(in_file, in_dict, out_file)
