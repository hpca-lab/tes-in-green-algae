# replaces sequence ids in GFF/BED files from a tab-separated dictionary
import sys
import csv
import re

if "snakemake" in locals():
    in_file    = str(snakemake.input.tsv)
    in_dict    = str(snakemake.input.dictionary)
    out_file   = str(snakemake.output.tsv)
else:
    if len(sys.argv) < 3: sys.exit("Needs three arguments: 1) input file, 2) input dictionary, 3) output file")
    in_file   = sys.argv[1]
    in_dict   = sys.argv[2]
    out_file  = sys.argv[3]


def main(in_file, in_dict, out_file):
    # slurp dictionary
    id_for = dict()
    with open(in_dict, "r") as infh:
        reader = csv.reader(infh, delimiter="\t")
        for row in reader:
            row[1] = re.sub(" .*$", "", row[1]) # remove everything after the initial ID
            id_for[row[0]] = row[1] # key in column 1, value (trimmed) in column 2


    with open(in_file, "r") as infh, open(out_file, "w") as outfh:
        reader = csv.reader(infh, delimiter="\t")
        writer = csv.writer(outfh, delimiter="\t")
        for row in reader:
            # just write out rows that have no fields
            if len(row) < 2:
                writer.writerow(row)
                continue
            # replace in non-comment rows
            if row[0][0] != "#":
                if row[0] in id_for:
                    row[0] = id_for[row[0]]
            writer.writerow(row)


if __name__ == "__main__":
    main(in_file, in_dict, out_file)
