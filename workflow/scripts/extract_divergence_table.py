# extract the divergence data from a divsum file created by calcDivergence.pl (util script from RepeatMasker)
# output is in tab-separated format

import sys
import csv

if "snakemake" in locals():
    in_file     = str(snakemake.input.divsum)
    genome_size = int(snakemake.params.genome_size)
    out_file    = str(snakemake.output.tsv)
else:
    if len(sys.argv) < 3:
        sys.exit("Needs three arguments: 1) input divsum file, 2) output file, 3) genome size in bp")
    in_file     = sys.argv[1]
    out_file    = sys.argv[2]
    genome_size = sys.argv[3]


def main(in_file, out_file, genome_size):
    with open(in_file, "r") as infh, open(out_file, "w") as outfh:
        reader = csv.reader(infh, delimiter=" ")
        writer = csv.writer(outfh, delimiter="\t")
        start = False
        for row in reader:
            if len(row) == 0: continue  # skip empty lines
            if row[0].startswith("Coverage"):
                start = True
                continue
            if start:
                if row[-1] == "":
                    row = row[:-1]  # -1 because trailing space creates empty field
                if row[0] != "Div":
                    for i in range(1, len(row)):
                        row[i] = 100 * float(row[i]) / genome_size
                writer.writerow(row)


if __name__ == "__main__":
    main(in_file, out_file, genome_size)
