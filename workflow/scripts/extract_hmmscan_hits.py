#!/usr/bin/env python

import sys
from Bio import SearchIO

if "snakemake" in locals():
    domtbl_file = str(snakemake.input[0])
    output_file = str(snakemake.input[1])
else:
    domtbl_file = str(sys.argv[1])
    output_file = str(sys.argv[2])

for qresult in SearchIO.parse(domtbl_file, "hmmer3-text"):
    print("{id} {descr}".format(id = qresult.id, descr = qresult.description))
