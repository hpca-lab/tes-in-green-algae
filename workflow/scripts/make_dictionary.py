#!/usr/bin/env python
"""
Replaces Fasta headers with sequential IDs, create a dictionary for subsequent replacement

Input: Fasta file

Output: 2 files:
    - file.dict: dictionary, records are in the format "header -> ID"
    - file.enum.fa: Fasta file with replaced headers
"""

import sys

if "snakemake" in locals():
    in_fasta    = str(snakemake.input.fasta)
    out_dict    = str(snakemake.output.dictionary)
    out_fasta   = str(snakemake.output.fasta)
else:
    in_fasta   = sys.argv[1]
    out_dict   = in_fasta + ".dict"
    out_fasta  = in_fasta + ".enum.fa"


def main(in_fasta, out_dict, out_fasta):
    id = 1
    with open(in_fasta, "r") as infh, open(out_dict, "w") as outfh_dict, open(out_fasta, "w") as outfh_fasta:
        for line in infh:
            if line[0] == ">":
                outfh_dict.write(f"{id}\t{line[1:]}")
                outfh_fasta.write(f">{id}\n")
                id = id + 1
            else:
                outfh_fasta.write(line)


if __name__ == "__main__":
    main(in_fasta, out_dict, out_fasta)
